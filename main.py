#/usr/bin/env python3
#
# Main class defition


class WindFarm(object):
    lifetime = 15
    leverage = 0.7
    investment_per_MW = 1500
    capacity = 20
    cpi = 0.03
    hrs_eq = 2500
    production = 2500 * 20
    tot_inv = investment_per_MW * capacity
    """
    CFADS = revenues + opex + working capital adjustments + VAT + Tax
    Because revenue and opex cash flows are calculated by working capital fct,
    CFADS are updated in WC fct, vat and tax fct.
    """
    s_cfads = []
    s_y_cfads = []
    s_wca_adj = []
    """
    time
    """
    start_debt = 2015
    nper_ops = lifetime * 4
    """
    Revenues
    """
    s_revenues_pl = []  # For P&L
    s_revenues_cf = []  # For Cash flow (after working capital adjustments)
    s_revenues_wc = []
    """
    Opex
    """
    s_costs_pl = []
    s_costs_cf = []
    s_costs_wc = []
    """
    Taxes
    """
    s_taxes = []
    # Value added tax:
    n_vat_opex = 0.20
    n_vat_rev = 0.20  # Placeholder in case they differ
    s_vat_wca_opex = []  # net outflow
    s_vat_wca_rev = []  # net
    s_vat_wca = []
    net_b_taxes = []
    net_a_taxes = []
    s_cumulated_result = []
    depreciation = 0
    s_depreciation = []
    """
    Debt
    """
    s_bop = []
    s_k = []
    s_cs_k = []
    s_i = []
    s_eop = []
    y_bop = []
    y_k = []
    y_cs_k = []
    y_i = []
    y_eop = []
    y_total = []
    s_total = []
    s_hedged_rate = []
    s_y_hedged_rate = []
    s_y_dscr = []
    s_y_dscr_2yfwd = []
    """
    Equity
    """
    s_cfe = []
    flow = []

    def simple_print(self, alistname, alist, toggle):
        if toggle == 0:
            print(alistname, len(alist), alist[0::len(alist) - 1])
        elif toggle == 1:
            print(alistname, len(alist), alist[0:len(alist)])
        else:
            print("wrong toggle")

    def working_capital(self, inflow, outflow, wc_balance, payment_delay):
        """
        inflow: list of payable or receivable
        outflow: empty, recipient list
        payment delay: delay in months
        """
        x = (3 - payment_delay) / 3
        for i in range(self.nper_ops):
            y = inflow[i] * x  # Amount paid/received this period
            z = round(inflow[i] * (1 - x), 3)  # Amount NOT paid/received
            if i > 0:
                z1 = wc_balance[i - 1]
            else:
                z1 = 0
            outflow.append(round(y + z1, 3))
            wc_balance.append(z)

    def cfads_calc(self):
        """
            + revenues CF
            - opex CF
            - taxes CF - Corality DOES include taxes in CFADS calc.
            == CFADS
        """
        for i in range(self.nper_ops):
            # ((i + 1) // 4 - (i // 4)) prints at the end of every 4 period
            # (4th quarter). Can/will be tweaked to adjust tax payment schedule
            # though atm it is ugly and unwieldy.
            x = self.s_revenues_cf[i] + self.s_costs_cf[i]
            # ((i + 1) // 4 - (i // 4)) * self.s_taxes[i // 4]
            self.s_cfads.append(x)
        # Annualize for reference
        for i in range(self.lifetime):
            z = sum(self.s_cfads[i * 4:i * 4 + 4])
            self.s_y_cfads.append(z)

    def cfads_check(self, toggle):
        print("==== CFADS check ====")
        k = sum(self.s_revenues_cf)
        l = sum(self.s_costs_cf)
        m = sum(self.s_taxes)
        n = sum(self.s_cfads)
        o = sum(self.s_y_cfads)
        if toggle == 1:
            print("rev:", k)
            print("costs:", l)
            print("taxes:", m)
            print("y CFADS:", o)
        if round(n) == round(k + l + m):
            print("CFADS:", n)
            print("CFADS = revenues + opex + taxes", "\n")
        else:
            print("Differential:", n - (k + l + m), "\n")


# Pretty Print table in tabular format
# From the intertubes (ActiveState) with own string formatting
def PrettyPrint(table, justify="R", columnWidth=10):
    # Not enforced but
    # if provided columnWidth must be greater than max column width in table!
#    if columnWidth == 0:
#        # find max column width
#        for row in table:
#            for col in row:
#                width = len(str(col))
#                if width > columnWidth:
#                    columnWidth = width
    outputStr = ""
    for row in table:
        rowList = []
        for col in row:
            if justify == "R":  # justify right
                rowList.append(str('{:.2f}'.format(col)).rjust(columnWidth))
            elif justify == "L":  # justify left
                rowList.append(str('{:.2f}'.format(col)).ljust(columnWidth))
            elif justify == "C":  # justify center
                rowList.append(str('{:.2f}'.format(col)).center(columnWidth))
        outputStr += ' '.join(rowList) + "\n"
    print(outputStr)
