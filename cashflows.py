#/usr/bin/env python3
#
# Cash Flow Class
import main
from numpy import irr


class CashFlows(main.WindFarm):

    def cash_flow(self):
        """
        CF:
            + revenues (cf)
            - costs (cf)
            - debt interests
            - debt capital
            - taxes
            == net cash flow
        """
        rev_cf = []
        debt_t = []
        op = []
        x = - round(self.tot_inv * (1 - self.leverage), 3)
        self.flow.append(x)
        for i in range(self.lifetime):
            # Agregate into yearly values, except taxes
            # taxes is already yearly
            rev_cf.append(sum(self.s_revenues_cf[i * 4:i * 4 + 4]))
            debt_t.append(sum(self.s_total[i * 4:i * 4 + 4]))
            op.append(sum(self.s_costs_cf[i * 4:i * 4 + 4]))
            # Add to list
            self.flow.append(rev_cf[i] + self.s_taxes[i] + op[i] + debt_t[i])

        # PrettyPrint([rev_cf, op, debt_t, self.s_taxes, self.flow[1:]])

    def irr(self):
        x = irr(self.flow)
        print("IRR:", '{0:.2%}'.format(x), "\n")
