#/usr/bin/env python3
#
# bankdebt
import main
import datetime
from numpy import ppmt, round, mean


class BankDebt(main.WindFarm):
    s_euribor = []

    def __init__(self, margin, maturity):
        self.total = self.tot_inv
        self.margin = margin
        self.maturity = maturity
        self.nper_d = maturity * 4

    def hedging(self, hedged_rate, coverage, duration):
        for i in range(self.nper_d):
            self.s_euribor.append(0.02)
        """ Calculates the number of days per quarter for debt duration """
        y = []
        m = []
        d = 1
        s_quarter_length = []
        """ Range is (0, length + 1) because we need (length) intervals,
        not (length) start dates """
        for i in range(self.nper_d + 1):
            y.append(self.start_debt + i // 4)
            m.append((i % 4) * 3 + 1)
            if i > 0:
                x = datetime.date(y[i], m[i], d) - datetime.date(y[i - 1],
                                                                 m[i - 1], d)
                s_quarter_length.append(x.days)
        """
        Calculates hedged interest rates per quarter for _duration_,
        self.maturity otherwise
        """
        for i in range(self.nper_d):
            x = s_quarter_length[i] / 365  # Ratio for quarter adjustment
            if i < duration * 4:
                y = coverage * hedged_rate + (1 - coverage) * self.s_euribor[i]
                z = round(x * (y + self.margin), 3)
                self.s_hedged_rate.append(z)
            else:
                k = round(x * (self.s_euribor[i] + self.margin), 3)
                self.s_hedged_rate.append(k)
        """
        Calculate yearly interest rate (hedged) for reference
        """
        for i in range(self.maturity):
            l = sum(self.s_hedged_rate[i * 4:i * 4 + 4])
            self.s_y_hedged_rate.append(l)
        """
        Consistency check -- inside function to access local variables
        """
        n = datetime.date(int(self.start_debt + self.maturity), 1, 1) - \
            datetime.date(int(self.start_debt), 1, 1)
        #print("total length diff:", n.days - sum(s_quarter_length))
        o = sum(self.s_hedged_rate[0:duration * 4]) / duration
        p = sum(self.s_hedged_rate[duration * 4:]) / \
            (self.maturity - duration)
        print("==== Interest rate check ====")
        print("Average number of days in quarter:", round(n.days /
              (self.maturity * 4)))
        print("Margin:", self.margin, "|| Euribor[1]:",
              self.s_euribor[1], "|| Hedge:", hedged_rate)
        print("Average yearly rate during hedging:", o)
        print("Average yearly rate after hedging:", p, "\n")

    def s_debt_cst(self):
        self.s_bop.append(self.leverage * self.tot_inv)
        for i in range(self.nper_d):
            k = round(ppmt(self.s_hedged_rate[i], 1, self.maturity * 4 - i,
                           self.s_bop[i]), 0)
            l = - round(self.s_hedged_rate[i] * self.s_bop[i])
            self.s_k.append(k)
            self.s_i.append(l)
            self.s_eop.append(self.s_bop[i] + k)
            self.s_total.append(k + l)
            if i < self.nper_d - 1:
                self.s_bop.append(self.s_eop[i])

    def s_debt_cst_K(self):
        self.s_bop.append(self.leverage * self.tot_inv)
        k = - self.s_bop[0] / self.nper_d
        for i in range(self.nper_d):
            self.s_k.append(k)
            l = - self.s_hedged_rate[i] * self.s_bop[i]
            self.s_i.append(l)
            self.s_eop.append(self.s_bop[i] + k)
            self.s_total.append(k + l)
            if i < self.nper_d - 1:
                self.s_bop.append(self.s_eop[i])

    def s_debt_cst_cs(self, cs):
        """
            Cash sweep with mandatory repayment as a % of CFADS, every quarter
            Remaining scheduled capital payment remain at the original maturity,
            will change.
        """
        fcf = 0
        # Initialiizes outstanding bank debt
        self.s_bop.append(self.leverage * self.tot_inv)
        for i in range(self.maturity * 4):
            if self.s_bop[i] > 0:
                k = round(ppmt(self.s_hedged_rate[i], 1, self.maturity * 4 - i,
                               self.s_bop[i]), 0)
                l = - round(self.s_hedged_rate[i] * self.s_bop[i])
            else:
                k = 0
                l = 0
            self.s_i.append(l)
            # Now, check how much is left from CFADS, and sweep it.
            fcf = self.s_cfads[i] + k + l
            if fcf > 0:
                x = round(cs * fcf)  # Cash sweep capital pmt
                y = (self.s_bop[i] + k)  # D. after K pmt
                k += - min(x, y)
                self.s_cs_k.append(- min(x, y))
            else:
                self.s_cs_k.append(0)
            self.s_k.append(k)
            self.s_total.append(k + l)
            self.s_eop.append(self.s_bop[i] + self.s_k[i])
            if i < self.nper_d - 1:
                self.s_bop.append(self.s_eop[i])

    def match_ops(self):
        """
            Matches length of debt schedules with the project's.
            * find length mismatch
            * append 0s to debt schedules
        """
        x = self.lifetime - self.maturity
        if x > 0:
            for i in range(x * 4):
                self.s_bop.append(0)
                self.s_k.append(0)
                self.s_cs_k.append(0)
                self.s_i.append(0)
                self.s_eop.append(0)
                self.s_total.append(0)

    def debt_check(self, toggle):
        print("==== Debt Check ====")
        a = sum(self.s_k)
        a1 = - self.tot_inv * self.leverage
        if a == a1:
            print("Total capital flows:", a, "(equals initial debt)")
        b = sum(self.s_i)
        print("Total interest flows:", b)
        c = (self.s_eop.index(0) + 1) / 4
        print("Year repaid:", c)
        """
            ratios      """
        r1 = min(self.s_y_dscr)
        r2 = mean(self.s_y_dscr)
        r3 = max(self.s_y_dscr)
        print("Ratios min:", round(r1, 3), "|| avg:", round(r2, 3),
              "|| max:", round(r3, 3))
        y1 = self.s_y_dscr.index(r1)
        y3 = self.s_y_dscr.index(r3)
        print("Min year:", y1, "|| Max year:", y3)
        d = min(self.s_eop)
        print("No overpayment:", d, "\n")
        # Annualize debt flows
        for i in range(self.lifetime):
            x = self.s_bop[i * 4]
            y = sum(self.s_k[i * 4:i * 4 + 4])
            y1 = sum(self.s_cs_k[i * 4:i * 4 + 4])
            z = sum(self.s_i[i * 4:i * 4 + 4])
            m = self.s_eop[i * 4 + 3]
            n = sum(self.s_total[i * 4:i * 4 + 4])
            self.y_bop.append(x)
            self.y_k.append(y)
            self.y_cs_k.append(y1)
            self.y_i.append(z)
            self.y_eop.append(m)
            self.y_total.append(n)
        if toggle == 1:
            main.PrettyPrint([self.y_bop, self.y_k, self.y_cs_k,
                              self.y_i, self.y_total, self.y_eop])

    def ratio(self):
        """
            Find DSCRs
            * Annualizes k+i payments
        """
        # Annualized interest and capital payments
        # 12 months backward looking
        for i in range(self.maturity):
            x = sum(self.s_k[i * 4:i * 4 + 4])
            y = sum(self.s_i[i * 4:i * 4 + 4])
            r1 = - self.s_y_cfads[i] / (x + y)
            self.s_y_dscr.append(r1)
        # 2 year forward looking
            if i < self.maturity - 1:
                k = sum(self.s_k[i * 4:i * 4 + 8])
                l = sum(self.s_i[i * 4:i * 4 + 8])
                r2 = - sum(self.s_y_cfads[i:i + 2]) / (k + l)
            self.s_y_dscr_2yfwd.append(r2)
