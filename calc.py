#/usr/bin/env python3
#
# Calculations
import main
import bankdebt
import revenues
import opex
import taxes
import balancesheet
import cashflows
import shlDebt

wf = main.WindFarm()

""" Initialize Revenues """
rev = revenues.Revenues(-0.005, 90)
rev.cpi_index()
rev.prod()
rev.working_capital(rev.s_revenues_pl, rev.s_revenues_cf, rev.s_revenues_wc, 2)

""" Initialize Costs -- per MW and MWh, respectively """
costs = opex.Opex(50000, 0.25)
costs.working_capital(costs.s_costs_pl, costs.s_costs_cf, costs.s_costs_wc, 1)

""" Initialize Debt """
debt = bankdebt.BankDebt(0.04, 15)
debt.hedging(0.03, 1.0, 12)  # (swap, coverage, duration)

""" CFADS """
wf.cfads_calc()
wf.cfads_check(0)

""" Calculate debt """
#debt.s_debt_cst_cs(0.1)
#debt.s_debt_cst_K()
debt.s_debt_cst()
debt.ratio()
#debt.match_ops()
debt.debt_check(1)

""" Initializes P&L (taxes) """
tax = taxes.Taxes(0.25)

tax.linear_depreciation()
tax.calc_taxes()
tax.cumulated_result()

""" Initializes CashFlows"""
cashflows = cashflows.CashFlows()
cashflows.cash_flow()
cashflows.irr()

""" Balance Sheet """
bls = balancesheet.BalanceSheet()
bls.balance_sheet()

""" Shareholder equity """
shl = shlDebt.ShlEquity(.6)
shl.shl_debt()
