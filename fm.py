#/usr/bin/env python
#import time
import datetime
from numpy import ppmt, round, irr, mean

"""
's_': a s_chedule, i.e. a list of values over the lifetime of the project

"""


# Pretty Print table in tabular format
# From the intertubes (ActiveState) with own string formatting
def PrettyPrint(table, justify="R", columnWidth=10):
    # Not enforced but
    # if provided columnWidth must be greater than max column width in table!
#    if columnWidth == 0:
#        # find max column width
#        for row in table:
#            for col in row:
#                width = len(str(col))
#                if width > columnWidth:
#                    columnWidth = width
    outputStr = ""
    for row in table:
        rowList = []
        for col in row:
            if justify == "R":  # justify right
                rowList.append(str('{:.2f}'.format(col)).rjust(columnWidth))
            elif justify == "L":  # justify left
                rowList.append(str('{:.2f}'.format(col)).ljust(columnWidth))
            elif justify == "C":  # justify center
                rowList.append(str('{:.2f}'.format(col)).center(columnWidth))
        outputStr += ' '.join(rowList) + "\n"
    print(outputStr)


class WindFarm(object):
    lifetime = 15
    leverage = 0.7
    investment_per_MW = 1500
    capacity = 20
    cpi = 0.03
    hrs_eq = 2500
    production = 2500 * 20
    tot_inv = investment_per_MW * capacity
    """
    CFADS = revenues + opex + working capital adjustments + VAT + Tax
    Because revenue and opex cash flows are calculated by working capital fct,
    CFADS are updated in WC fct, vat and tax fct.
    """
    s_cfads = []
    s_y_cfads = []
    s_wca_adj = []
    """
    time
    """
    start_debt = 2015
    nper_ops = lifetime * 4
    """
    Revenues
    """
    s_revenues_pl = []  # For P&L
    s_revenues_cf = []  # For Cash flow (after working capital adjustments)
    s_revenues_wc = []
    """
    Opex
    """
    s_costs_pl = []
    s_costs_cf = []
    s_costs_wc = []
    """
    Taxes
    """
    s_taxes = []
    # Value added tax:
    n_vat_opex = 0.20
    n_vat_rev = 0.20  # Placeholder in case they differ
    s_vat_wca_opex = []  # net outflow
    s_vat_wca_rev = []  # net
    s_vat_wca = []
    net_b_taxes = []
    net_a_taxes = []
    s_cumulated_result = []
    depreciation = 0
    s_depreciation = []
    """
    Debt
    """
    s_bop = []
    s_k = []
    s_i = []
    s_eop = []
    s_total = []
    s_hedged_rate = []
    s_y_hedged_rate = []
    s_y_dscr = []
    s_y_dscr_2yfwd = []
    """
    Returns
    """
    flow = []

    def simple_print(self, alistname, alist, toggle):
        if toggle == 0:
            print(alistname, len(alist), alist[0::len(alist) - 1])
        elif toggle == 1:
            print(alistname, len(alist), alist[0:len(alist)])
        else:
            print("wrong toggle")

    def working_capital(self, inflow, outflow, wc_balance, payment_delay):
        """
        inflow: list of payable or receivable
        outflow: empty, recipient list
        payment delay: delay in months
        """
        x = (3 - payment_delay) / 3
        for i in range(self.nper_ops):
            y = inflow[i] * x  # Amount paid/received this period
            z = round(inflow[i] * (1 - x), 3)  # Amount NOT paid/received
            if i > 0:
                z1 = wc_balance[i - 1]
            else:
                z1 = 0
            outflow.append(round(y + z1, 3))
            wc_balance.append(z)

    def cfads_calc(self):
        """
            + revenues CF
            - opex CF
            ||| taxes CF - Corality does not include taxes, Novenergia does.
            == CFADS
        """
        for i in range(self.nper_ops):
            # ((i + 1) // 4 - (i // 4)) prints at the end of every 4 period
            # (4th quarter). Can/will be tweaked to adjust tax payment schedule
            # though atm it is ugly and unwieldy.
            x = self.s_revenues_cf[i] + self.s_costs_cf[i]
            #    ((i + 1) // 4 - (i // 4)) * s_taxes...
            self.s_cfads.append(x)
        # Annualize for reference
        for i in range(self.lifetime):
            z = sum(self.s_cfads[i * 4:i * 4 + 4])
            self.s_y_cfads.append(z)

    def cfads_check(self, toggle):
        print("==== CFADS check ====")
        k = sum(self.s_revenues_cf)
        l = sum(self.s_costs_cf)
        # m = sum(self.s_taxes)
        n = sum(self.s_cfads)
        o = sum(self.s_y_cfads)
        if toggle == 1:
            print("rev:", k)
            print("costs:", l)
            # print("taxes:", m)
            print("y CFADS:", o)
        if round(n) == round(k + l) and round(o) == round(n):
            print("CFADS:", n, "\n")
        else:
            print("Differential:", n - (k + l), "\n")


class BankDebt(WindFarm):
    s_euribor = []

    def __init__(self, margin, maturity):
        self.total = self.tot_inv
        self.margin = margin
        self.maturity = maturity
        self.nper_d = maturity * 4

    def hedging(self, hedged_rate, coverage, duration):
        for i in range(self.nper_d):
            self.s_euribor.append(0.02)
        """ Calculates the number of days per quarter for debt duration """
        y = []
        m = []
        d = 1
        s_quarter_length = []
        """ Range is (0, length + 1) because we need (length) intervals,
        not (length) start dates """
        for i in range(self.nper_d + 1):
            y.append(self.start_debt + i // 4)
            m.append((i % 4) * 3 + 1)
            if i > 0:
                x = datetime.date(y[i], m[i], d) - datetime.date(y[i - 1],
                                                                 m[i - 1], d)
                s_quarter_length.append(x.days)
        """
        Calculates hedged interest rates per quarter for _duration_,
        self.maturity otherwise
        """
        for i in range(self.nper_d):
            x = s_quarter_length[i] / 365  # Ratio for quarter adjustment
            if i < duration * 4:
                y = coverage * hedged_rate + (1 - coverage) * self.s_euribor[i]
                z = round(x * (y + self.margin), 3)
                self.s_hedged_rate.append(z)
            else:
                k = round(x * (self.s_euribor[i] + self.margin), 3)
                self.s_hedged_rate.append(k)
        """
        Calculate yearly interest rate (hedged) for reference
        """
        for i in range(self.maturity):
            l = sum(self.s_hedged_rate[i * 4:i * 4 + 4])
            self.s_y_hedged_rate.append(l)
        """
        Consistency check -- inside function to access local variables
        """
        n = datetime.date(int(self.start_debt + self.maturity), 1, 1) - \
            datetime.date(int(self.start_debt), 1, 1)
        #print("total length diff:", n.days - sum(s_quarter_length))
        o = sum(self.s_hedged_rate[0:duration * 4]) / duration
        p = sum(self.s_hedged_rate[duration * 4:]) / \
            (self.maturity - duration)
        print("==== Interest rate check ====")
        print("Average number of days in quarter:", round(n.days /
              (self.maturity * 4)))
        print("Margin:", self.margin, "|| Euribor[1]:",
              self.s_euribor[1], "|| Hedge:", hedged_rate)
        print("Average yearly rate during hedging:", o)
        print("Average yearly rate after hedging:", p, "\n")

    def s_debt_cst(self):
        self.s_bop.append(self.leverage * self.tot_inv)
        for i in range(self.nper_d):
            k = round(ppmt(self.s_hedged_rate[i], 1, self.maturity * 4 - i,
                           self.s_bop[i]), 0)
            l = - round(self.s_hedged_rate[i] * self.s_bop[i])
            self.s_k.append(k)
            self.s_i.append(l)
            self.s_eop.append(self.s_bop[i] + k)
            self.s_total.append(k + l)
            if i < self.nper_d - 1:
                self.s_bop.append(self.s_eop[i])

    def s_debt_cst_K(self):
        self.s_bop.append(self.leverage * self.tot_inv)
        k = - self.s_bop[0] / self.nper_d
        for i in range(self.nper_d):
            self.s_k.append(k)
            l = - self.s_hedged_rate[i] * self.s_bop[i]
            self.s_i.append(l)
            self.s_eop.append(self.s_bop[i] + k)
            self.s_total.append(k + l)
            if i < self.nper_d - 1:
                self.s_bop.append(self.s_eop[i])

    def s_debt_cst_cs(self, cs):
        """
            Cash sweep with mandatory repayment as a % of CFADS, every quater
        """
        self.fcf = []
        # Initialiizes outstanding bank debt
        self.s_bop.append(self.leverage * self.tot_inv)
        for i in range(self.maturity * 4):
            if self.s_bop[i] > 0:
                k = round(ppmt(self.s_hedged_rate[i], 1, self.maturity * 4 - i,
                               self.s_bop[i]), 0)
                l = - round(self.s_hedged_rate[i] * self.s_bop[i])
            else:
                k = 0
                l = 0
            self.s_k.append(k)
            self.s_i.append(l)
            """ Begin Cash sweep """
            # FCF should probably calculated elsewhere
            self.fcf.append(self.s_cfads[i] + k + l)
            if self.fcf[i] > 0:
                # self.simple_print("s_k:", self.s_k, 1)
                x = round(cs * self.fcf[i])  # Cash sweep capital pmt
                y = (self.s_bop[i] + self.s_k[i])  # D. after K pmt
                self.s_k[i] += - min(x, y)
                """ the above is equivalent to:
                if y >= x:
                    self.s_k[i] += - x
                else:
                    self.s_k[i] += -y
            End Cash sweep """
            self.s_total.append(self.s_i[i] + self.s_k[i])
            self.s_eop.append(self.s_bop[i] + self.s_k[i])
            if i < self.nper_d - 1:
                self.s_bop.append(self.s_eop[i])

    def match_ops(self):
        """
            Matches length of debt schedules with the project's.
            * find length mismatch
            * append 0s to debt schedules
        """
        x = self.lifetime - self.maturity
        if x > 0:
            for i in range(x * 4):
                self.s_bop.append(0)
                self.s_k.append(0)
                self.s_i.append(0)
                self.s_eop.append(0)
                self.s_total.append(0)

    def debt_check(self, toggle):
        print("==== Debt Check ====")
        a = sum(self.s_k)
        a1 = - self.tot_inv * self.leverage
        if a == a1:
            print("Total capital flows:", a, "(equals initial debt)")
        b = sum(self.s_i)
        print("Total interest flows:", b)
        c = (self.s_eop.index(0) + 1) / 4
        print("Year repaid:", c)
        """
            ratios      """
        r1 = min(self.s_y_dscr)
        r2 = mean(self.s_y_dscr)
        r3 = max(self.s_y_dscr)
        print("Ratios min:", round(r1, 3), "|| avg:", round(r2, 3),
              "|| max:", round(r3, 3))
        y1 = self.s_y_dscr.index(r1)
        y3 = self.s_y_dscr.index(r3)
        print("Min year:", y1, "|| Max year:", y3)
        d = min(self.s_eop)
        print("No overpayment:", d, "\n")
        if toggle == 1:
            self.simple_print("s_bop:", self.s_bop, 0)
            self.simple_print("s_k:", self.s_k, 0)
            self.simple_print("s_i:", self.s_i, 0)
            self.simple_print("s_total:", self.s_total, 0)
            self.simple_print("s_eop:", self.s_eop, 0)

    def ratio(self):
        """
            Find DSCRs
            * Annualizes k+i payments
        """
        # Annualized interest and capital payments
        # 12 months backward looking
        for i in range(self.maturity):
            x = sum(self.s_k[i * 4:i * 4 + 4])
            y = sum(self.s_i[i * 4:i * 4 + 4])
            r1 = - self.s_y_cfads[i] / (x + y)
            self.s_y_dscr.append(r1)
        # 2 year forward looking
            if i < self.maturity - 1:
                k = sum(self.s_k[i * 4:i * 4 + 8])
                l = sum(self.s_i[i * 4:i * 4 + 8])
                r2 = - sum(self.s_y_cfads[i:i + 2]) / (k + l)
            self.s_y_dscr_2yfwd.append(r2)


class Revenues(WindFarm):
    prices = []
    s_adj_cpi_index = []
    s_production = []

    def __init__(self, cpi_diff, price_0):
        self.cpi_diff = cpi_diff
        self.price_0 = price_0

    def cpi_index(self):
        for i in range(0, self.lifetime):
            self.s_adj_cpi_index.append((1 + self.cpi + self.cpi_diff) ** i)

    def prod(self):  # Seasonal variations + cpi indexation
        seasonality = [0.25, 0.25, 0.25, 0.25]
        for i in range(0, self.nper_ops):
            x = self.production * seasonality[i % 4]
            y = (self.s_adj_cpi_index[i // 4] * self.price_0)
            z = round(x * y / 1000, 3)
            self.s_production.append(x)
            self.prices.append(y)
            self.s_revenues_pl.append(z)


class Opex(WindFarm):
    inv_working_capital = []
    fixed = []
    variable = []
    """
        fixed: cost per MW/year
        variable: cost per MWh
    """

    def __init__(self, fixed_costs, variable_costs):
        self.fixed_costs = fixed_costs
        self.variable_costs = variable_costs
        for i in range(self.nper_ops):
            x = (-self.fixed_costs * self.capacity / 1000 / 4) * \
                (1 + self.cpi) ** (i // 4)
            y = - self.variable_costs * self.production / 1000 * \
                (1 + self.cpi) ** (i // 4)
            z = round(x + y, 3)
            self.fixed.append(x)
            self.variable.append(y)
            self.s_costs_pl.append(z)


class Taxes(WindFarm):
    y_rev_pl = []
    y_debt_i = []
    y_opex_pl = []

    def __init__(self, rate):
        self.rate = rate

    def linear_depreciation(self):
        self.depreciation = - self.tot_inv / self.lifetime
        for i in range(self.lifetime):
            self.s_depreciation.append(self.depreciation)

    def calc_taxes(self):
        for i in range(self.lifetime):
            # Agregate into yearly values
            self.y_rev_pl.append(sum(self.s_revenues_pl[i * 4:i * 4 + 4]))
            self.y_debt_i.append(sum(self.s_i[i * 4:i * 4 + 4]))
            self.y_opex_pl.append(sum(self.s_costs_pl[i * 4:i * 4 + 4]))
            # Calc net_before_taxes
            x = self.y_rev_pl[i] + self.y_debt_i[i] + \
                self.y_opex_pl[i] + self.depreciation
            # Calc net_after_taxes
            if x > 0:
                y = - round(self.rate * x, 3)
            else:
                y = 0
            self.net_b_taxes.append(x)
            self.net_a_taxes.append(x + y)
            # WindFarm object variable update
            self.s_taxes.append(y)
        # PrettyPrint([self.y_rev_pl, self.y_opex_pl, self.y_debt_i,
        #             self.s_depreciation, self.net_b_taxes,  self.s_taxes,
        #             self.net_a_taxes])

    def cumulated_result(self):
        """
        Yearly:
            + net result of the previous period
            + sum of previous values
            - dividend distribution
            == cumulated result
        """
        x = 0
        # First cumulated result is always 0
        self.s_cumulated_result.append(0)
        for i in range(1, self.lifetime):
            x += self.net_a_taxes[i - 1]  # placeholder for sub of dividends
            self.s_cumulated_result.append(x)


class CashFlows(WindFarm):

    def cash_flow(self):
        """
        CF:
            + revenues (cf)
            - costs (cf)
            - debt interests
            - debt capital
            - taxes
            == net cash flow
        """
        rev_cf = []
        debt_t = []
        op = []
        x = - round(self.tot_inv * (1 - self.leverage), 3)
        self.flow.append(x)
        for i in range(self.lifetime):
            # Agregate into yearly values, except taxes
            # taxes is already yearly
            rev_cf.append(sum(self.s_revenues_cf[i * 4:i * 4 + 4]))
            debt_t.append(sum(self.s_total[i * 4:i * 4 + 4]))
            op.append(sum(self.s_costs_cf[i * 4:i * 4 + 4]))
            # Add to list
            self.flow.append(rev_cf[i] + self.s_taxes[i] + op[i] + debt_t[i])

        # PrettyPrint([rev_cf, op, debt_t, self.s_taxes, self.flow[1:]])

    def irr(self):
        x = irr(self.flow)
        print("IRR:", x * 100, "\n")


class BalanceSheet(WindFarm):

    """
    Balance Sheet:
        Assets:
            Fixed assets:
                Fixed assets -- total investment
                Intangible assets -- none here
                Depreciation -- depends on regimen, linear here atm
            Current assets:
                Current assets -- working capital inv. from rev.
                Tax credit -- none here atm
                Bank accounts (margin + DSRA) -- none here atm
            Total

        Liabilities:
            Debt:
                Bank -- debt outstanding at END of period
                Shareholder -- none here atm
            Short-term liabilities:
                Current payables -- working capital inv. from opex
                VAT payables -- none here atm. Net of rev. and opex.
                Tax payables -- none here atm
                Interest payables -- from shareholder debt
            Equity:
                Share capital -- Shareholder structure for later
                Legal reserve -- Dividend distribution constraint
                Cumulated result
                Net result for the period
            Total
    """

    def balance_sheet(self):
        # Vars
        # Assets
        s_fixed_assets = []
        s_current_assets = []
        s_assets = []
        # Bank accounts
        cf = 0
        s_cash = []
        # Liabilities
        s_bank_debt_eop = []
        s_current_payables = []
        s_equity = []
        s_net_result = []
        s_liabilities = []
        s_diff = []
        s_rolling_diff = []

        # Assets:
        for i in range(self.lifetime):
            # Assets:
            #   Fixed assets:
            x = self.tot_inv + (i + 1) * self.s_depreciation[i]
            s_fixed_assets.append(x)
            #   Current assets
            #   Current assets -- working capital inv. from rev.
            y = self.s_revenues_wc[i * 4]
            s_current_assets.append(y)
            #   Tax credit -- none here atm
            #   Bank accounts (margin + DSRA) -- none here atm
            cf += self.flow[i + 1]
            s_cash.append(cf)
            # Total
            s_assets.append(x + y + s_cash[i])
            # Liabilities:
        # Debt:
            #   Bank -- debt outstanding at END of period
            z = self.s_eop[i * 4 + 3]
            s_bank_debt_eop.append(z)
            #   Shareholder -- none here atm
            # Short-term liabilities:
            #   Current payables -- working capital inv. from opex
            #       here, liabilities are positive (they go toward the larger
            #       position)
            k = - self.s_costs_wc[i * 4]
            s_current_payables.append(k)
            #   VAT payables -- none here atm. Net of rev. and opex.
            #   Tax payables -- none here atm
            #   Interest payables -- from shareholder debt
            # Equity:
            #   Share capital -- Shareholder structure for later
            e = (1 - self.leverage) * self.tot_inv
            s_equity.append(e)
            #   Legal reserve -- Dividend distribution constraint
            #   Cumulated result
            c = self.s_cumulated_result[i]
            #   Net result for the period
            m = self.net_a_taxes[i]
            s_net_result.append(m)
        # Total
            s_liabilities.append(z + k + e + c + m)

        # Difference for the period
            diff = s_assets[i] - s_liabilities[i]
            s_diff.append(diff)
        # Rolling diff of diff
            if i == 0:
                # Rolling diff has no value in first period
                s_rolling_diff.append(0)
            else:
                rdiff = diff - s_diff[i - 1]
                s_rolling_diff.append(rdiff)

        # Checks:
        if round(sum(s_diff)) != 0:
            print("Assets")
            PrettyPrint([s_fixed_assets, s_current_assets, s_cash, s_assets])
            print("Liabilities")
            PrettyPrint([s_bank_debt_eop, s_current_payables, s_equity,
                         self.s_cumulated_result, s_net_result, s_liabilities])
            print("Diffs")
            PrettyPrint([s_diff, s_rolling_diff])
            print()
        else:
            print("==== Balance Sheet check ====")
            print("Phew, passed", "\n")

wf = WindFarm()
""" Initialize Debt """

""" Initialize Revenues """
rev = Revenues(-0.005, 90)
rev.cpi_index()
rev.prod()
rev.working_capital(rev.s_revenues_pl, rev.s_revenues_cf, rev.s_revenues_wc, 2)

""" Initialize Costs -- per MW and MWh, respectively """
costs = Opex(50000, 0.25)
costs.working_capital(costs.s_costs_pl, costs.s_costs_cf, costs.s_costs_wc, 1)

""" Calculate debt """
debt = BankDebt(0.04, 15)
debt.hedging(0.03, 1.0, 12)  # (swap, coverage, duration)
#debt.s_debt_cst_K()
debt.s_debt_cst()
#debt.match_ops()

""" Initializes P&L (taxes) """
tax = Taxes(0.25)
tax.linear_depreciation()
tax.calc_taxes()
tax.cumulated_result()

""" CFADS """
wf.cfads_calc()
wf.cfads_check(0)

""" """
#debt.s_debt_cst_cs(0.1)
debt.ratio()
debt.debt_check(0)

""" Initializes CashFlows"""
cashflows = CashFlows()
cashflows.cash_flow()
cashflows.irr()

""" Balance Sheet """
bls = BalanceSheet()
bls.balance_sheet()
