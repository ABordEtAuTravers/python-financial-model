#/usr/bin/env python3
#
# Profit and Losses, Taxes class
import main


class Taxes(main.WindFarm):
    y_rev_pl = []
    y_opex_pl = []

    def __init__(self, rate):
        self.rate = rate

    def linear_depreciation(self):
        self.depreciation = - self.tot_inv / self.lifetime
        for i in range(self.lifetime):
            self.s_depreciation.append(self.depreciation)

    def calc_taxes(self):
        for i in range(self.lifetime):
            # Agregate into yearly values
            self.y_rev_pl.append(sum(self.s_revenues_pl[i * 4:i * 4 + 4]))
            self.y_opex_pl.append(sum(self.s_costs_pl[i * 4:i * 4 + 4]))
            # Calc net_before_taxes
            x = self.y_rev_pl[i] + self.y_i[i] + \
                self.y_opex_pl[i] + self.depreciation
            # Calc net_after_taxes
            if x > 0:
                y = - round(self.rate * x, 3)
            else:
                y = 0
            self.net_b_taxes.append(x)
            self.net_a_taxes.append(x + y)
            # main.WindFarm object variable update
            self.s_taxes.append(y)
        # PrettyPrint([self.y_rev_pl, self.y_opex_pl, self.y_debt_i,
        #             self.s_depreciation, self.net_b_taxes,  self.s_taxes,
        #             self.net_a_taxes])

    def cumulated_result(self):
        """
        Yearly:
            + net result of the previous period
            + sum of previous values
            - dividend distribution
            == cumulated result
        """
        x = 0
        # First cumulated result is always 0
        self.s_cumulated_result.append(0)
        for i in range(1, self.lifetime):
            x += self.net_a_taxes[i - 1]  # placeholder for sub of dividends
            self.s_cumulated_result.append(x)
