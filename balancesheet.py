#/usr/bin/env python3
#
# Balance Sheet Class
import main


class BalanceSheet(main.WindFarm):

    """
    Balance Sheet:
        Assets:
            Fixed assets:
                Fixed assets -- total investment
                Intangible assets -- none here
                Depreciation -- depends on regimen, linear here atm
            Current assets:
                Current assets -- working capital inv. from rev.
                Tax credit -- none here atm
                Bank accounts (margin + DSRA) -- none here atm
            Total

        Liabilities:
            Debt:
                Bank -- debt outstanding at END of period
                Shareholder -- none here atm
            Short-term liabilities:
                Current payables -- working capital inv. from opex
                VAT payables -- none here atm. Net of rev. and opex.
                Tax payables -- none here atm
                Interest payables -- from shareholder debt
            Equity:
                Share capital -- Shareholder structure for later
                Legal reserve -- Dividend distribution constraint
                Cumulated result
                Net result for the period
            Total
    """

    def balance_sheet(self):
        # Vars
        # Assets
        s_fixed_assets = []
        s_current_assets = []
        s_assets = []
        # Bank accounts
        cf = 0
        s_cash = []
        # Liabilities
        s_bank_debt_eop = []
        s_current_payables = []
        s_equity = []
        s_net_result = []
        s_liabilities = []
        s_diff = []
        s_rolling_diff = []

        # Assets:
        for i in range(self.lifetime):
            # Assets:
            #   Fixed assets:
            x = self.tot_inv + (i + 1) * self.s_depreciation[i]
            s_fixed_assets.append(x)
            #   Current assets
            #   Current assets -- working capital inv. from rev.
            y = self.s_revenues_wc[i * 4]
            s_current_assets.append(y)
            #   Tax credit -- none here atm
            #   Bank accounts (margin + DSRA) -- none here atm
            cf += self.flow[i + 1]
            s_cash.append(cf)
            # Total
            s_assets.append(x + y + s_cash[i])
            # Liabilities:
        # Debt:
            #   Bank -- debt outstanding at END of period
            z = main.WindFarm.s_eop[i * 4 + 3]
            s_bank_debt_eop.append(z)
            #   Shareholder -- none here atm
            # Short-term liabilities:
            #   Current payables -- working capital inv. from opex
            #       here, liabilities are positive (they go toward the larger
            #       position)
            k = - self.s_costs_wc[i * 4]
            s_current_payables.append(k)
            #   VAT payables -- none here atm. Net of rev. and opex.
            #   Tax payables -- none here atm
            #   Interest payables -- from shareholder debt
            # Equity:
            #   Share capital -- Shareholder structure for later
            e = (1 - self.leverage) * self.tot_inv
            s_equity.append(e)
            #   Legal reserve -- Dividend distribution constraint
            #   Cumulated result
            c = self.s_cumulated_result[i]
            #   Net result for the period
            m = self.net_a_taxes[i]
            s_net_result.append(m)
        # Total
            s_liabilities.append(z + k + e + c + m)

        # Difference for the period
            diff = s_assets[i] - s_liabilities[i]
            s_diff.append(diff)
        # Rolling diff of diff
            if i == 0:
                # Rolling diff has no value in first period
                s_rolling_diff.append(0)
            else:
                rdiff = diff - s_diff[i - 1]
                s_rolling_diff.append(rdiff)

        # Checks:
        if round(sum(s_diff)) != 0:
            print("Assets")
            main.PrettyPrint([s_fixed_assets, s_current_assets, s_cash,
                              s_assets])
            print("Liabilities")
            main.PrettyPrint([s_bank_debt_eop, s_current_payables, s_equity,
                              self.s_cumulated_result, s_net_result,
                              s_liabilities])
            print("Diffs")
            main.PrettyPrint([s_diff, s_rolling_diff])
            print()
        else:
            print("==== Balance Sheet check ====")
            print("Phew, passed", "\n")
