#/usr/bin/env python3
#
# Revenue Class
import main


class Revenues(main.WindFarm):
    prices = []
    s_adj_cpi_index = []
    s_production = []

    def __init__(self, cpi_diff, price_0):
        self.cpi_diff = cpi_diff
        self.price_0 = price_0

    def cpi_index(self):
        for i in range(0, self.lifetime):
            self.s_adj_cpi_index.append((1 + self.cpi + self.cpi_diff) ** i)

    def prod(self):  # Seasonal variations + cpi indexation
        seasonality = [0.25, 0.25, 0.25, 0.25]
        for i in range(0, self.nper_ops):
            x = self.production * seasonality[i % 4]
            y = (self.s_adj_cpi_index[i // 4] * self.price_0)
            z = round(x * y / 1000, 3)
            self.s_production.append(x)
            self.prices.append(y)
            self.s_revenues_pl.append(z)
