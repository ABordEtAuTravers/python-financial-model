#/usr/bin/env python3
#
# Operations and Maintenance Class
import main


class Opex(main.WindFarm):
    inv_working_capital = []
    fixed = []
    variable = []
    """
        fixed: cost per MW/year
        variable: cost per MWh
    """

    def __init__(self, fixed_costs, variable_costs):
        self.fixed_costs = fixed_costs
        self.variable_costs = variable_costs
        for i in range(self.nper_ops):
            x = (-self.fixed_costs * self.capacity / 1000 / 4) * \
                (1 + self.cpi) ** (i // 4)
            y = - self.variable_costs * self.production / 1000 * \
                (1 + self.cpi) ** (i // 4)
            z = round(x + y, 3)
            self.fixed.append(x)
            self.variable.append(y)
            self.s_costs_pl.append(z)
