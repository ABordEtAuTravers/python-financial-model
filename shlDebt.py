#/usr/bin/env python3
#
# Shareholder equty structure
import main
import datetime
from numpy import ppmt, round, mean


class ShlEquity(main.WindFarm):
    shl_d = 0
    shl_bop = []
    shl_i = []
    shl_k = []
    shl_eop = []
    shl_int = 0.03  # Interest on Shareholder loan

    def __init__(self, shl_ratio):
        self.shl_ratio = shl_ratio
        self.shl_d = self.tot_inv * self.leverage * self.shl_ratio
        self.shl_bop.append(self.shl_d)
        print(self.shl_bop[0])

    def shl_debt(self):
        """ Fastest repayment possible of Shareholder loans from CFADS,
            after senior loan cash sweep """
        """
            + CFADS
            - shareholder loan interests
            - max capital repayments (no schedule)
        """
        """ TODOLIST
            0. find available CF
            1. period based calculation
                - interests for the period
                - pay them
                    - if you can
                    - if you can't, add to capital
                - pay capital if you can
            2. Outputs:
                interests to be paid
        """
        # Calculate CF available for equity distribution
        for i in range(self.nper_ops):
            k = self.s_cfads[i] + self.s_i[i] + self.s_k[i]
            self.s_cfe.append(round(k))

        # Main loop
        l = 0
        k = 0
        m = 0
        print(self.nper_ops)
        for i in range(self.nper_ops):
            # Interests are calculated quarterly
            print(i, self.shl_bop[i])
            if self.shl_bop[i] > 0:
                l += (self.shl_int / 4) * self.shl_bop[i]
                # Trigger if we reach end of year (assuming a Q1 start)
                if i > 0 and i % 4 == 0:
                    # Cash from the preceding year
                    cf = sum(self.s_cfe[i - 3:i + 1])
                    # interests
                    i_paid = - min(l, cf)
                    l += i_paid  # interest outstanding to capitalize
                    # update cf after interest payments
                    cf += i_paid
                    # capital
                    k = - min(self.shl_bop[i], cf)
                    m = self.shl_bop[i] + k + l
                    self.shl_eop.append(m)
                    self.shl_bop.append(m)
                else:
                    self.shl_bop.append(self.shl_bop[i])
                    self.shl_eop.append(self.shl_bop[i])





